/*
 * adxl345.h
 *
 *  Created on: Mar 21, 2024
 *      Author: macuser
 */
#include "main.h"
#ifndef INC_ADXL345_H_
#define INC_ADXL345_H_


extern DMA_HandleTypeDef hdma_usart2_rx;
//extern DMA_HandleTypeDef hdma_usart2_tx;


extern uint8_t i2c_tx[];
extern char str_enc_usb[];
extern char str_Adxl345[];
extern char str_count[];


extern uint8_t fl_rx;

//extern uint8_t RxBuf[RxBuf_SIZE];
extern char  MainBuf_Rx[MainBuf_Rx_SIZE];//MainBuf_Rx[MainBuf_Rx_SIZE];

extern uint8_t TxBuf[TxBuf_SIZE];
extern char  MainBuf_Tx[MainBuf_Tx_SIZE];

//+++++++++++++++++++++++++++++++++++++++++++++

uint8_t AdxlFlgInt1 = 0;
uint8_t AdxlFlgInt2 = 0;


struct AdxlCommands
{
	uint8_t	DEVID;	/*	Device ID	*/
	uint8_t	THRESH_TAP;	/*	Tap threshold	*/
	uint8_t	OFSX;	/*	X-axis offset	*/
	uint8_t	OFSY;	/*	Y-axis offset	*/
	uint8_t	OFSZ;	/*	Z-axis offset	*/
	uint8_t	DUR;	/*	Tap duration	*/
	uint8_t	Latent;	/*	Tap latency	*/
	uint8_t	Window;	/*	Tap window	*/
	uint8_t	THRESH_ACT;	/*	Activity threshold	*/
	uint8_t	THRESH_INACT;	/*	Inactivity threshold	*/
	uint8_t	TIME_INACT;	/*	Inactivity time	*/
	uint8_t	ACT_INACT_CTL;	/*	Axis enable control for activity and inactivity detection	*/
	uint8_t	THRESH_FF;	/*	Free-fall threshold	*/
	uint8_t	TIME_FF;	/*	Free-fall time	*/
	uint8_t	TAP_AXES;	/*	Axis control for single tap/double tap	*/
	uint8_t	ACT_TAP_STATUS;	/*	Source of single tap/double tap	*/
	uint8_t	BW_RATE;	/*	Data rate and power mode control	*/
	uint8_t	POWER_CTL;	/*	Power-saving features control	*/
	uint8_t	INT_ENABLE;	/*	Interrupt enable control	*/
	uint8_t	INT_MAP;	/*	Interrupt mapping control	*/
	uint8_t	INT_SOURCE;	/*	Source of interrupts	*/
	uint8_t	DATA_FORMAT;	/*	Data format control	*/
	uint8_t	DATAX0;	/*	X-Axis Data 0	*/
	uint8_t	DATAX1;	/*	X-Axis Data 1	*/
	uint8_t	DATAY0;	/*	Y-Axis Data 0	*/
	uint8_t	DATAY1;	/*	Y-Axis Data 1	*/
	uint8_t	DATAZ0;	/*	Z-Axis Data 0	*/
	uint8_t	DATAZ1;	/*	Z-Axis Data 1	*/
	uint8_t	FIFO_CTL;	/*	FIFO control	*/
	uint8_t	FIFO_STATUS;	/*	FIFO status	*/
};

struct AdxlCommands AdxlReg = {0x00, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22,
	0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,
	0x2E, 0x2F, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39};

enum AdxlBitNum
{
	D0,
	D1,
	D2,
	D3,
	D4,
	D5,
	D6,
	D7
};

struct AdxlData
{
	uint8_t id;
	int16_t x,y,z;

	double xg, yg, zg;

	uint8_t activity;

	uint8_t int_src;

	uint8_t raw[6];
}Adxl345;

#define SWODEBUG 1

#define SCALE_FACTOR 29.0

#define ADXL_ADR (0x53 << 1)
//+++++++++++++++++++++++++++++++++++++++++++++

void adxl345_init();

void adxl345_read();

#endif /* INC_ADXL345_H_ */
