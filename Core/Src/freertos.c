/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "usart.h"
#include "i2c.h"
#include <stdio.h>

#include <strings.h>
#include <string.h>


#include "stm32f4xx_it.h"
#include "usart.h"
#include "adxl345.h"   //-------------------------------------------

#include "SHT21_h/sht2x_for_stm32_hal.h"

#include "bmp180h/bmp180.h"
//#include "BMP280_h/bmp280.h"

#include "MAX30102_h/max30102_for_stm32_hal.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>
#define RxBuf_SIZE 128
#define MainBuf_Rx_SIZE 128

//#define TxBuf_SIZE 128
//#define MainBuf_Tx_SIZE 128

//++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
//++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>

extern DMA_HandleTypeDef hdma_usart2_rx;
//extern DMA_HandleTypeDef hdma_usart2_tx;
extern UART_HandleTypeDef huart2;

extern I2C_HandleTypeDef hi2c1;  //I2C_HandleTypeDef hi2c1;

//extern SPI_HandleTypeDef hspi2;


extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
//++++++++++++++++++++++++++++++++++++++202302022000++++++++++++>>>>

//char str[] = "\r\n \r\n 2023 06 11 21 45_iMac \r\n\r\n  ";
//char str[] = "\r\n \r\n 2023 12 28 16 48_MBP17 \r\n\r\n  ";
//char str[] = "\r\n \r\n 2023 12 31 11 38_MBP17 \r\n\r\n  ";

//char str[96] = "\r\n \r\n 2024 03 24 21 24 MBP17  +++++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 01 17 38 MBP17  +++++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 07 14 00 MBP17  +++++++\r\n\r\n  ";
//
//char str[96] = "\r\n \r\n 2024 04 12 21 00 MBP17  +++task1++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 19 06 00 MBP17  +++task1++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 20 21 00 MBP17  +++task1++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 21 18 11 MBP17  +++task1++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 27 06 00 MBP17  +++++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 27 21 54 MBP17  +++++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 28 10 00 MBP17  +++++++\r\n\r\n  ";
//char str[96] = "\r\n \r\n 2024 04 30 21 50 MBP17  +++++++\r\n\r\n  ";
char str[96] = "\r\n \r\n 2024 05 01 14 50 MBP17  +++++++\r\n\r\n  ";

///bmp180+++++++++++++++++++++++++++++++++++++++++++
float Temperature;
float Pressure;
float Altitude;

///bmp180+++++++++++++++++++++++++++++++++++++++++++

//extern uint8_t str[];

//char str_r_n[4] = "\r\n";

//char str_enc_usb[32] = "4096"; // ++++++++  202302012022
//
uint8_t i2c_tx[2];
//char str_enc_usb[256];
//extern uint8_t i2c_tx[];


char str_enc_usb_f[128];

char str_enc_usb_r[128];



uint32_t enc = 300;
uint32_t enc1;
uint32_t x;


extern uint8_t fl_rx;

//extern uint8_t RxBuf[RxBuf_SIZE];
extern char  MainBuf_Rx[MainBuf_Rx_SIZE];//MainBuf_Rx[MainBuf_Rx_SIZE];


//extern uint8_t TxBuf[TxBuf_SIZE];
extern char  MainBuf_Tx[MainBuf_Tx_SIZE];
//++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>

char trans_str[96];

//char transPID_str1[64];
char trans_str_encoder[32];

extern uint32_t enc ;

//uint16_t adc_to_pid1, adc_to_pid2, adc_to_pid3, adc_to_pid4 = 0;
uint16_t adc1, adc2 ,adc3, adc4, adc5, adc6 = 0;

//uint16_t adc[4] = {0};

//uint16_t count_main = 600;
//+++++++++++_____________-----------------------------------------
//extern uint8_t i2c_tx[2];
extern char str_count[];

//++++++++++++______________----------------------------------------
uint16_t ccr1_ccr4_timer3_x = 1000;  //  ccr2_ccr4_timer3_x >>> 0-:- 10000
//uint16_t ccr1_ccr4_timer3_x_max = 9000;

//uint16_t rpm_arr3 = 1000;
// //+++
//uint16_t freg_mosfet = 0;
//uint16_t freg_mosfet_max = 100;

uint16_t rpm_arr3_max = 4000;

uint16_t count_main_real_rpm = 20;
uint16_t count_real_rpm = 1;

uint16_t count_main_command = 1000;
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
uint16_t speed_rpm_arr_tim2 = 2100;
uint16_t k_pulse_tim2 = 300;//speed_rpm_arr_tim2/2;
uint16_t step_tim2 = 300;

uint16_t speed_rpm_arr_tim3 = 4000;
uint16_t k_pulse_tim3 = 100;//speed_rpm_arr_tim3/2;
uint16_t step_tim3 = 100;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//uint16_t delay_pc13 = 1;//3  1  5
//uint16_t rpm_set = 3;
//
//uint16_t d1 = 10;
//uint16_t d0 = 20;

//uint16_t test_param = 1;

uint16_t speed_rpm_arr_tim2_Adxl345_xg = 1500;
uint16_t speed_rpm_arr_tim2_Adxl345_xg_raw = 1500;


uint16_t *fields[128] = {0}; //+++++++++++++++fields[64]+++++++************* * *********** *fields[96]*+++++++++++++++2024 04 19 10 22=====

//+++++++++++++++++++++++++++++++++++++++++++++++++++++202312281722++++++
float temp = 0;
uint8_t hold = 0;


//BMP280_HandleTypedef bmp280;

float pressure, temperature, humidity;

uint16_t size;
uint8_t Data[256];

//+++++++++++++++++++++++++++++++++++++++++

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityHigh7,
};
/* Definitions for myTask02 */
osThreadId_t myTask02Handle;
uint32_t myTask02Buffer[ 512 ];
osStaticThreadDef_t myTask02ControlBlock;
const osThreadAttr_t myTask02_attributes = {
  .name = "myTask02",
  .cb_mem = &myTask02ControlBlock,
  .cb_size = sizeof(myTask02ControlBlock),
  .stack_mem = &myTask02Buffer[0],
  .stack_size = sizeof(myTask02Buffer),
  .priority = (osPriority_t) osPriorityHigh,
};
/* Definitions for myTask03 */
osThreadId_t myTask03Handle;
uint32_t myTask03Buffer[ 128 ];
osStaticThreadDef_t myTask03ControlBlock;
const osThreadAttr_t myTask03_attributes = {
  .name = "myTask03",
  .cb_mem = &myTask03ControlBlock,
  .cb_size = sizeof(myTask03ControlBlock),
  .stack_mem = &myTask03Buffer[0],
  .stack_size = sizeof(myTask03Buffer),
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for myTask04 */
osThreadId_t myTask04Handle;
const osThreadAttr_t myTask04_attributes = {
  .name = "myTask04",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for myQueue01 */
osMessageQueueId_t myQueue01Handle;
const osMessageQueueAttr_t myQueue01_attributes = {
  .name = "myQueue01"
};
/* Definitions for myQueue02 */
osMessageQueueId_t myQueue02Handle;
const osMessageQueueAttr_t myQueue02_attributes = {
  .name = "myQueue02"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void StartTask02(void *argument);
void StartTask03(void *argument);
void StartTask04(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of myQueue01 */
  myQueue01Handle = osMessageQueueNew (16, sizeof(uint16_t), &myQueue01_attributes);

  /* creation of myQueue02 */
  myQueue02Handle = osMessageQueueNew (16, sizeof(uint16_t), &myQueue02_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of myTask02 */
  myTask02Handle = osThreadNew(StartTask02, NULL, &myTask02_attributes);

  /* creation of myTask03 */
  myTask03Handle = osThreadNew(StartTask03, NULL, &myTask03_attributes);

  /* creation of myTask04 */
  myTask04Handle = osThreadNew(StartTask04, NULL, &myTask04_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  uint16_t l = 0;
  uint16_t a = 20;
  uint16_t b = 400;


  HAL_UART_Transmit(&huart2, (uint8_t*)str, strlen(str), 1000);
//  osDelay(100);//++++++++++++
//
// osDelay(200);
//  CDC_Transmit_FS(str_usb, strlen(str_usb));
//
//


  osDelay(5000);

  /* Infinite loop */
  for(;;)
  {
 			  if (fl_rx > 0)
 			  {
  //				void  decode_command(char *MainBuf)
			  fl_rx = 0;
//		{
				  fields[l] = strtok(&MainBuf_Rx,",");

				  while(fields[l] != NULL) {
					  l++;
					  fields[l] = strtok(NULL, ",");
				  }
					  if (strcmp (fields[0], "S") == 0)
					  {
						  speed_rpm_arr_tim2 = atoi(fields[1]);

//						  speed_rpm_arr_tim2 = atoi(fields[1],strlen(fields[1]));
						  k_pulse_tim2 =  speed_rpm_arr_tim2/2;                         //atoi(fields[2], strlen(fields[2]));
						  step_tim2 = atoi(fields[2]);

						  speed_rpm_arr_tim3 = atoi(fields[3]);
						  k_pulse_tim3 = speed_rpm_arr_tim3/2;                          //atoi(fields[4], strlen(fields[4]));
						  step_tim3 = atoi(fields[4]);
					  }
					  osDelay(50);
	    	  sprintf(str_count, "\r\n rpm_arr_tim2 = %d k_pulse_tim2 = %d rpm_arr_tim3 = %d k_pulse_tim3 = %d\r\n\r\n",
	    			  speed_rpm_arr_tim2, step_tim2, speed_rpm_arr_tim3, step_tim3);

	    	  HAL_UART_Transmit(&huart2, (uint8_t*)&str_count, strlen(str_count), 1000);

//	    	  str_count[128] = 0;
//	    	  osDelay(10);
//	    	  CDC_Transmit_FS((uint8_t*)str_count, strlen(str_count));

	    	  for(int i = 0; i <= MainBuf_Rx_SIZE; i++)
			  	  {
				  	  MainBuf_Rx[i] = 0;
			  	  }
 			  }
//
//    	  HAL_TIM_Base_Stop_IT(&htim2);
//    	  count_main_real_rpm = __HAL_TIM_GET_COUNTER(&htim2);
//    	  __HAL_TIM_SET_COUNTER(&htim2, 0x0000);
//    	  HAL_TIM_Base_Start_IT(&htim2);


//    	  count_real_rpm = count_main_real_rpm;
//

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 				Inf   from  ADC     >>>>  for      timer    arr reg
//
// 			  	in_min = 0
//				in_max = 15000
// 			  	out_min = 300
//				out_max = 9000
//
// 			   x = Adxl345_xg;
//
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 		     /*
 			  (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
 			 */

 		      /* map(val,0,4096,10,999); */
//
// 		//        freq_rpm = (adc1 - 150)*(900 - 150)/(4096-0)+150;
//
// 		        freq_rpm = (adc1 - 700)*(4000 - 700)/(4096 - 0)+ 700;
//
// 		//          freq_rpm = adc1 -100;
// 			 speed_rpm_arr_tim2_Adxl345_xg_raw =  ((speed_rpm_arr_tim2_Adxl345_xg_raw + 15000) * (9000 + 300))/((16000 + 16000) + 300);
//
// 			 speed_rpm_arr_tim2_Adxl345_xg = speed_rpm_arr_tim2_Adxl345_xg_raw;
// 				speed_rpm_arr_tim2_Adxl345_xg_raw = Adxl345.xg;


 			 speed_rpm_arr_tim2_Adxl345_xg =  (speed_rpm_arr_tim2_Adxl345_xg_raw - 0) * (16000 - 0) / (16000 - 0) + 0;

// 			 speed_rpm_arr_tim2_Adxl345_xg = speed_rpm_arr_tim2_Adxl345_xg_raw;

 			 //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



//			TIM2->ARR = speed_rpm_arr_tim2;

//			k_pulse_tim2 = a * count_real_rpm + b;
			// Pulse >  y = ax + b    >> a = 20    x = count_main_real_rpm b = 400

//			if(k_pulse_tim2 >= 3900){ k_pulse_tim2 = 4001;}
//			if(k_pulse_tim2 <= 1000){ k_pulse_tim2 = 0;}

//			if(k_pulse_tim2 >= 9000){ k_pulse_tim2 = 9000;}   //2024 04 19 15 51
//			if(k_pulse_tim2 <= 300){ k_pulse_tim2 = 300;}  //2024 04 19 15 5

			if(speed_rpm_arr_tim2_Adxl345_xg >= 9000){ speed_rpm_arr_tim2_Adxl345_xg = 9000;}   //2024 04 21 20 00
			if(speed_rpm_arr_tim2_Adxl345_xg <= 600){ speed_rpm_arr_tim2_Adxl345_xg = 600;}  	//2024 04 21 20 00


//			TIM2->CCR1 = speed_rpm_arr_tim2_Adxl345_xg/2;     //PA15  >>>>>>>>>>>>
 			TIM2->ARR = speed_rpm_arr_tim2_Adxl345_xg;
			TIM2->CCR1 = 300;     //PA15  >>>>>>>>>>>>

//			TIM2->CCR1 = k_pulse_tim2/;     //PA15  >>>>>>>>>>>>

//			TIM2->CCR2 = k_pulse_tim2;
//			TIM2->CCR3 = k_pulse_tim2;

//			TIM3->ARR = speed_rpm_arr_tim3;
//			TIM3->ARR =
//			if(k_pulse_tim3 >= 3900){ k_pulse_tim3 = 4001;}
//			if(k_pulse_tim3 <= 1000){ k_pulse_tim3 = 0;}

			if(k_pulse_tim3 >= 9000){ k_pulse_tim3 = 9000;}
			if(k_pulse_tim3 <= 300){ k_pulse_tim3 = 300;}

			TIM3->CCR1 = k_pulse_tim3;     //PB4 >>>>>>>>>>>>>
//			TIM3->CCR2 = k_pulse_tim3;
//			TIM3->CCR3 = k_pulse_tim3;
//			TIM3->CCR4 = k_pulse_tim3;
//				if(count_real_rpm <= 700) {count_real_rpm = 0;}
//
//
//	    	TIM3->CCR3 = k_pulse;//   PB0   led  control MOSFET
//	    	TIM3->CCR4 = k_pulse;//   PB1   stok MOSFET
//
//	    	TIM3->CCR1 = 2048; //  ++++ 2023 12 29 16 28 +++++


//			 sprintf(str_count, "\r\n TEST  RPM= %d  Pulse = %d  delayPC13 = %d  param_TEST = %d\r\n",
//					 	 	 	 	 	 rpm_arr_tim3,  k_pulse,     delay_pc13,     test_param);
//			 HAL_UART_Transmit(&huart2, (uint8_t*)&str_count, strlen(str_count), 1000);

//			 osDelay(100);

//			 sprintf(str_count, "\r\n TEST  RPM= %d  Pulse = %d  delayPC13 = %d  param_TEST = %d\r\n",
//					 rpm_arr_tim3,  k_pulse,     delay_pc13,     test_param);

//			 CDC_Transmit_FS((uint8_t*)str_count, strlen(str_count));  //  ++++ 2023 12 29 16 28 +++++

   osDelay(100);



//	  osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void *argument)
{
  /* USER CODE BEGIN StartTask02 */

	i2c_tx[0] = AdxlReg.BW_RATE;
	i2c_tx[1] = ((1 << D3) | (1 << D1));
	HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)i2c_tx, 2, 1000);

	//	/* Range 2g */
	i2c_tx[0] = AdxlReg.DATA_FORMAT;
	i2c_tx[1] = ((0 << D0) | (0 << D1));
	HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)i2c_tx, 2, 1000);

	i2c_tx[0] = AdxlReg.POWER_CTL;
	i2c_tx[1] = (1 << D3);
	HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)i2c_tx, 2, 1000);

	osDelay(100);
	  float Adxl345_xg;
	  float Adxl345_yg;
	  float Adxl345_zg;

  /* Infinite loop */
  for(;;)
  {

		HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)&AdxlReg.DATAX0, 1, 100);
		HAL_I2C_Master_Receive(&hi2c1, ADXL_ADR, (uint8_t*)&Adxl345.raw, 6, 100);

//		osDelay(100);

		Adxl345.x = ((int16_t)(Adxl345.raw[1] << 8) | Adxl345.raw[0]);
		Adxl345.y = ((int16_t)(Adxl345.raw[3] << 8) | Adxl345.raw[2]);
		Adxl345.z = ((int16_t)(Adxl345.raw[5] << 8) | Adxl345.raw[4]);


	//	Adxl345.xg = (SCALE_FACTOR*Adxl345.x)/1000;
	//	Adxl345.yg = (SCALE_FACTOR*Adxl345.y)/1000;
	//	Adxl345.zg = (SCALE_FACTOR*Adxl345.z)/1000;

//		Adxl345.xg = (256*Adxl345.x)/1000;
//		Adxl345.yg = (256*Adxl345.y)/1000;
//		Adxl345.zg = (256*Adxl345.z)/1000;

		Adxl345.xg = 64*Adxl345.x;
		Adxl345.yg = 64*Adxl345.y;
		Adxl345.zg = 64*Adxl345.z;

		Adxl345_xg = Adxl345.xg;
		Adxl345_yg = Adxl345.yg;
		Adxl345_zg = Adxl345.zg;

		speed_rpm_arr_tim2_Adxl345_xg_raw = Adxl345.xg;


//		sprintf(MainBuf_Tx, "%6.f, %6.f, %6.f\n", Adxl345_xg, Adxl345_yg, Adxl345_zg);


//	  HAL_UART_Transmit(&huart2, (uint8_t*)&MainBuf_Tx, strlen(MainBuf_Tx), 1000);

	  osDelay(900);
  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_StartTask03 */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask03 */
void StartTask03(void *argument)
{
  /* USER CODE BEGIN StartTask03 */
	 __HAL_TIM_SET_COUNTER(&htim1, 100);   //------------------

	enc = 300;
	int count_0 = 0;
	int count_1 = 0;
  /* Infinite loop */
  for(;;)
  {
	  if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)){
		  count_0 = enc;
		 HAL_GPIO_WritePin( GPIOC, GPIO_PIN_13, 0);
		 osDelay(count_0);
		 HAL_GPIO_WritePin( GPIOC, GPIO_PIN_13, 1);
		 osDelay(count_0);
	  }
	  else
	  {
		 count_1 = enc/4;
		 HAL_GPIO_WritePin( GPIOC, GPIO_PIN_13, 0);
		 osDelay(count_1);
		 HAL_GPIO_WritePin( GPIOC, GPIO_PIN_13, 1);
		 osDelay(count_1);
	  }


	  enc = __HAL_TIM_GetCounter(&htim1);   //---------------------
		  if(enc <= 10)
		  {
			  enc = 10;
			  __HAL_TIM_SET_COUNTER(&htim1, 20);
		  }

		  if(enc >= 300)
		  {
			  enc = 300;
			  __HAL_TIM_SET_COUNTER(&htim1, 290);
		  }



	  osDelay(100);
  }
  /* USER CODE END StartTask03 */
}

/* USER CODE BEGIN Header_StartTask04 */
/**
* @brief Function implementing the myTask04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask04 */
void StartTask04(void *argument)
{
  /* USER CODE BEGIN StartTask04 */

//	SHT2x_Init(I2C_HandleTypeDef *hi2c);
//
//	float temp = 0;
//	uint8_t hold = 0;
//
//
//	BMP280_HandleTypedef bmp280;
//
//	float pressure, temperature, humidity;
//
//	uint16_t size;
//	uint8_t Data[256];


//	SHT2x_Init(&hi2c1);
/*
	SHT2x_SetResolution(RES_14_12);
*/


//	SHT2x_SoftReset();

//	void bmp280_init_default_params(bmp280_params_t *params);


	  BMP180_Start();
//
//	bmp280_init_default_params(&bmp280.params);
//	bmp280.addr = BMP280_I2C_ADDRESS_0;
//	bmp280.i2c = &hi2c1;
//
//	while (!bmp280_init(&bmp280, &bmp280.params)) {
//		size = sprintf((char *)Data, "BMP280 initialization failed\n");
//		HAL_UART_Transmit(&huart2, Data, size, 1000);
//		osDelay(2000);
//	}
//	bool bme280p = bmp280.id == BME280_CHIP_ID;
//	size = sprintf((char *)Data, "BMP280: found %s\n", bme280p ? "BME280" : "BMP280");
//	HAL_UART_Transmit(&huart2, Data, size, 1000);
//
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++

	// Override plot function

//	void max30102_plot(uint32_t ir_sample, uint32_t red_sample)
//	{
//	    // printf("ir:%u\n", ir_sample);                  // Print IR only
//	    // printf("r:%u\n", red_sample);                  // Print Red only
//	    printf("ir:%u,r:%u\n", ir_sample, red_sample);    // Print IR and Red
//	}

	// MAX30102 object
/*
	max30102_t max30102;





	  // Initiation
	  max30102_init(&max30102, &hi2c1);
	  max30102_reset(&max30102);
	  max30102_clear_fifo(&max30102);
	  max30102_set_fifo_config(&max30102, max30102_smp_ave_8, 1, 7);

	  // Sensor settings
	  max30102_set_led_pulse_width(&max30102, max30102_pw_16_bit);
	  max30102_set_adc_resolution(&max30102, max30102_adc_2048);
	  max30102_set_sampling_rate(&max30102, max30102_sr_800);
	  max30102_set_led_current_1(&max30102, 6.2);
	  max30102_set_led_current_2(&max30102, 6.2);

	  // Enter SpO2 mode
	  max30102_set_mode(&max30102, max30102_spo2);
	  max30102_set_a_full(&max30102, 1);

	  // Initiate 1 temperature measurement
	  max30102_set_die_temp_en(&max30102, 1);
	  max30102_set_die_temp_rdy(&max30102, 1);

	  uint8_t en_reg[2] = {0};
	  max30102_read(&max30102, 0x00, en_reg, 1);

*/
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++


  /* Infinite loop */
  for(;;)
  {

//++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
		unsigned char buffer[100] = { 0 };
		/* Gets current temperature & relative humidity. * /
		float cel = SHT2x_GetTemperature(1);
		/* Converts temperature to degrees Fahrenheit and Kelvin * /
		float fah = SHT2x_CelsiusToFahrenheit(cel);
		float kel = SHT2x_CelsiusToKelvin(cel);
		float rh = SHT2x_GetRelativeHumidity(1);
		/* May show warning below. Ignore and proceed. * /
		sprintf(buffer,
				"%d.%dºC, %d.%dºF, %d.%d K, %d.%d%% RH\n",
				SHT2x_GetInteger(cel), SHT2x_GetDecimal(cel, 1),
				SHT2x_GetInteger(fah), SHT2x_GetDecimal(fah, 1),
				SHT2x_GetInteger(kel), SHT2x_GetDecimal(kel, 1),
				SHT2x_GetInteger(rh), SHT2x_GetDecimal(rh, 1));
		HAL_UART_Transmit(&huart2, buffer, strlen(buffer), 1000);
		osDelay(250);

/




//++++++++++++++++++++++++++++++++++++++++++++++
//	  temp = SHT2x_GetTemperature(hold);
//
//	  SHT2x_ReadUserReg();
//
//		sprintf(str, "temperature = %6.f\n", temp);
//
//
//	  HAL_UART_Transmit(&huart2, (uint8_t*)&str, strlen(str), 1000);
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//		osDelay(100);
//			while (!bmp280_read_float(&bmp280, &temperature, &pressure, &humidity)) {
//				size = sprintf((char *)Data,
//						"Temperature/pressure reading failed\n");
//				HAL_UART_Transmit(&huart2, Data, size, 1000);
//				osDelay(2000);
//			}
//
//			size = sprintf((char *)Data,"Pressure: %.2f Pa, Temperature: %.2f C",
//					pressure, temperature);
//			HAL_UART_Transmit(&huart2, Data, size, 1000);
//			if (bme280p) {
//				size = sprintf((char *)Data,", Humidity: %.2f\n", humidity);
//				HAL_UART_Transmit(&huart2, Data, size, 1000);
//			}
//
//			else {
//				size = sprintf((char *)Data, "\n");
//				HAL_UART_Transmit(&huart2, Data, size, 1000);
//			}
//			osDelay(2000);
*/

	  Temperature = BMP180_GetTemp();
	  Pressure = BMP180_GetPress(0);
	  Altitude = BMP180_GetAlt(0);


	  sprintf(MainBuf_Tx, "Temperature =%6.f  Pressure =%6.f  Altitude =%6.f\n", Temperature, Pressure, Altitude);
	  HAL_UART_Transmit(&huart2, (uint8_t*)&MainBuf_Tx, strlen(MainBuf_Tx), 1000);

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
		    if (max30102_has_interrupt(&max30102))
		    {
		      max30102_interrupt_handler(&max30102);
		    }
*/
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	  osDelay(1000);
  }
  /* USER CODE END StartTask04 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

