/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f4xx_it.h"



/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>

extern DMA_HandleTypeDef hdma_usart2_rx;
//extern UART_HandleTypeDef huart2;

//extern DMA_HandleTypeDef hdma_usart2_tx;

extern UART_HandleTypeDef huart2;


extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
//extern SPI_HandleTypeDef hspi2;
//++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>
//
//
//uint32_t enc = 300;
//uint32_t enc1;
//uint32_t x;
//
//
///* Flags */
//uint8_t AdxlFlgInt1 = 0;
//uint8_t AdxlFlgInt2 = 0;
//
//


//
//uint8_t i2c_tx[2];
//char str_Adxl345[128];

//
//int32_t Adxl345_xg = 0.0;
//int32_t Adxl345_yg = 0.0;
//int32_t Adxl345_zg = 0.0;

//
//float Adxl345_xg = 0.0;
//float Adxl345_yg = 0.0;
//float Adxl345_zg = 0.0;

char str_count[128] = {0};
//char str[96] = "\r\n \r\n 2024 03 24 21 24 MBP17  +++++++\r\n\r\n  ";
char str_Adxl345[128] = {0};

//char str_enc_usb[96] = {0};

//uint8_t i2c_tx[2];

//
//#define TxBuf_SIZE 128      //64
#define MainBuf_Tx_SIZE 128    //64   128  (   20230601 1400 )


//uint8_t fl_tx = 0;
//uint8_t TxBuf[128];

//uint8_t MainBuf[MainBuf_SIZE];
char  MainBuf_Tx[MainBuf_Tx_SIZE] = {0};

//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);




uint8_t fl_rx = 0;
uint8_t RxBuf_DMA[RxBuf_DMA_SIZE];
//uint8_t MainBuf[MainBuf_SIZE];
//char  MainBuf_Rx[MainBuf_Rx_SIZE];//------------------------
uint8_t  MainBuf_Rx[MainBuf_Rx_SIZE];//+++++++++++++++++++++++++++



/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>
//
//#define RxBuf_SIZE 128
//#define MainBuf_Rx_SIZE 128    // 128  (   20230601 1400 )
//



void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
	if (huart->Instance == USART2)
	{
		memcpy (MainBuf_Rx, RxBuf_DMA, Size);
		fl_rx = 1;
		HAL_UARTEx_ReceiveToIdle_DMA(&huart2, RxBuf_DMA, RxBuf_DMA_SIZE);
		__HAL_DMA_DISABLE_IT(&hdma_usart2_rx, DMA_IT_HT);
	}


}
//++++++++++++++++++++++++++++++++++++++++++++++++++202208011730++++++++++++>>>>

//++++++++++++++++++++++++++++++++++++++++++++++++++202403292000++++++++++++>>>>

//++++++++++++++++++++++++++++++++++++++++++++++++++202403292000++++++++++++>>>>


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  MX_ADC1_Init();
  MX_TIM3_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  //++++++++++++++++++++++++++++++++++++++++++++++++++208011730++++++++++++>>>>

      HAL_UARTEx_ReceiveToIdle_DMA(&huart2, RxBuf_DMA, RxBuf_DMA_SIZE);
      __HAL_DMA_DISABLE_IT(&hdma_usart2_rx, DMA_IT_HT);
      //++++++++++++++++++++++++++++++++++++++++++++++++++202208011730++++++++++++>>>>

      //++++++++++++++++++++++++++++++++++++++++++++++++++202403292000++++++++++++>>>>

 //     HAL_StatusTypeDef HAL_UART_Transmit_DMA(UART_HandleTypeDef *huart, const uint8_t *pData, uint16_t Size)
//      HAL_UART_Transmit_DMA(UART_HandleTypeDef *huart, const uint8_t *pData, uint16_t Size);




      //++++++++++++++++++++++++++++++++++++++++++++++++++202403292000++++++++++++>>>>
      HAL_TIM_Base_Start(&htim2);
      HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
      HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
      HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);


      HAL_TIM_Base_Start(&htim3);
      HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);
      HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2);
      HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_3);
      HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_4);

      HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);  //------------

//      HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);  //-------------


  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* Call init function for freertos objects (in cmsis_os2.c) */
  MX_FREERTOS_Init();

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM9 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM9) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
