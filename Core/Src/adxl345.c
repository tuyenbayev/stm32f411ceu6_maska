/*
 * adxl345.c
 *
 *  Created on: Mar 21, 2024
 *      Author: macuser
 */
//#include "main.h"

#include "stm32f4xx_it.h"
#include "usart.h"
#include "i2c.h"

//#include "adxl345.h"
#include <stdio.h>
//



//#include <strings.h>
//#include <string.h>

//#include "stm32f4xx_it.h"

#include <strings.h>
//#include <stringlib.h>
#include "string.h"
#include <string.h>

////
////
//float Adxl345_xg;
//float Adxl345_yg;
//float Adxl345_zg;

void adxl345_init()
{

////	  sprintf(str_enc_usb, "\r\n adxl345 init    ++++++ \r\n");
////
////	  CDC_Transmit_FS((uint8_t*)str_enc_usb, strlen(str_enc_usb));
//
//	i2c_tx[0] = AdxlReg.BW_RATE;
//	i2c_tx[1] = ((1 << D3) | (1 << D1));
//	HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)i2c_tx, 2, 1000);
////	osDelay(1000);
////
////	/* Range 16g */
////	i2c_tx[0] = AdxlReg.DATA_FORMAT;
////	i2c_tx[1] = ((1 << D0) | (1 << D1));
////	HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)i2c_tx, 2, 1000);
//
////	/* Range 2g */
//	i2c_tx[0] = AdxlReg.DATA_FORMAT;
//	i2c_tx[1] = ((0 << D0) | (0 << D1));
//	HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)i2c_tx, 2, 1000);
/////	osDelay(1000);
////	/* Measure */
//	i2c_tx[0] = AdxlReg.POWER_CTL;
//	i2c_tx[1] = (1 << D3);
//	HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)i2c_tx, 2, 1000);
//

}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void adxl345_read() {
//
//		HAL_I2C_Master_Transmit(&hi2c1, ADXL_ADR, (uint8_t*)&AdxlReg.DATAX0, 1, 100);
//		HAL_I2C_Master_Receive(&hi2c1, ADXL_ADR, (uint8_t*)&Adxl345.raw, 6, 100);
//
////		osDelay(100);
//
//		Adxl345.x = ((int16_t)(Adxl345.raw[1] << 8) | Adxl345.raw[0]);
//		Adxl345.y = ((int16_t)(Adxl345.raw[3] << 8) | Adxl345.raw[2]);
//		Adxl345.z = ((int16_t)(Adxl345.raw[5] << 8) | Adxl345.raw[4]);
//
//
//	//	Adxl345.xg = (SCALE_FACTOR*Adxl345.x)/1000;
//	//	Adxl345.yg = (SCALE_FACTOR*Adxl345.y)/1000;
//	//	Adxl345.zg = (SCALE_FACTOR*Adxl345.z)/1000;
//
////		Adxl345.xg = (256*Adxl345.x)/1000;
////		Adxl345.yg = (256*Adxl345.y)/1000;
////		Adxl345.zg = (256*Adxl345.z)/1000;
//
//		Adxl345.xg = 64*Adxl345.x;
//		Adxl345.yg = 64*Adxl345.y;
//		Adxl345.zg = 64*Adxl345.z;
//
//		Adxl345_xg = Adxl345.xg;
//		Adxl345_yg = Adxl345.yg;
//		Adxl345_zg = Adxl345.zg;
//
//		Adxl345_xg = 1000.0;
//		Adxl345_yg = 2000.0;
//		Adxl345_zg = 3000.0;
//

//	#if(SWODEBUG)
	//	count = count + 1;

	//	sprintf(str_Adxl345, " xg = %d   yg = %d   zg = %.d   ", Adxl345.xg, Adxl345.yg, Adxl345.zg);
	//	CDC_Transmit_FS(str_Adxl345, strlen(str_Adxl345));

	//	sprintf(str_Adxl345, " xg = %6.f   yg = %6.f   zg = %6.f   ", Adxl345.xg, Adxl345.yg, Adxl345.zg);

//		sprintf(str_Adxl345, "%6.f, %6.f, %6.f\n", Adxl345.xg, Adxl345.yg, Adxl345.zg);

//		sprintf(str_Adxl345, "%6.f, %6.f, %6.f\n", Adxl345_xg, Adxl345_yg, Adxl345_zg);

//		sprintf(str_Adxl345, "%d %d %d\n", Adxl345_xg, Adxl345_yg, Adxl345_zg);
//		osDelay(5);
//		CDC_Transmit_FS(str_Adxl345, strlen(str_Adxl345));
//		#endif

//		sprintf(MainBuf_Tx, "%6.f, %6.f, %6.f\n", Adxl345_xg, Adxl345_yg, Adxl345_zg);
//		  HAL_UART_Transmit(&huart2, (uint8_t*)&MainBuf_Tx, strlen(MainBuf_Tx), 1000);

//		MainBuf_Tx = str_Adxl345;

		//		sprintf(str_Adxl345, "%d %d %d\n", Adxl345_xg, Adxl345_yg, Adxl345_zg);
//		osDelay(50);
//		CDC_Transmit_FS(str_Adxl345, strlen(str_Adxl345));
//  	  HAL_UART_Transmit(&huart2, (uint8_t*)&str_Adxl345, strlen(&str_Adxl345), 1000);
//  	  HAL_UART_Transmit(&huart2, (uint8_t*)&MainBuf_Tx, strlen(MainBuf_Tx), 1000);
//  	char str[96] = "\r\n \r\n 2024 04 10 22 00 MBP17  >+uart2+<+++\r\n\r\n  ";

//    HAL_UART_Transmit(&huart2, (uint8_t*)&str_1, strlen(str_1), 1000);


}

