################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (12.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/CCS811_Basic_c/CCS811_Basic.c 

OBJS += \
./Core/Src/CCS811_Basic_c/CCS811_Basic.o 

C_DEPS += \
./Core/Src/CCS811_Basic_c/CCS811_Basic.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/CCS811_Basic_c/%.o Core/Src/CCS811_Basic_c/%.su Core/Src/CCS811_Basic_c/%.cyclo: ../Core/Src/CCS811_Basic_c/%.c Core/Src/CCS811_Basic_c/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-CCS811_Basic_c

clean-Core-2f-Src-2f-CCS811_Basic_c:
	-$(RM) ./Core/Src/CCS811_Basic_c/CCS811_Basic.cyclo ./Core/Src/CCS811_Basic_c/CCS811_Basic.d ./Core/Src/CCS811_Basic_c/CCS811_Basic.o ./Core/Src/CCS811_Basic_c/CCS811_Basic.su

.PHONY: clean-Core-2f-Src-2f-CCS811_Basic_c

