################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (12.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/bmp180c/BMP180.c 

OBJS += \
./Core/Src/bmp180c/BMP180.o 

C_DEPS += \
./Core/Src/bmp180c/BMP180.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/bmp180c/%.o Core/Src/bmp180c/%.su Core/Src/bmp180c/%.cyclo: ../Core/Src/bmp180c/%.c Core/Src/bmp180c/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F411xE -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-bmp180c

clean-Core-2f-Src-2f-bmp180c:
	-$(RM) ./Core/Src/bmp180c/BMP180.cyclo ./Core/Src/bmp180c/BMP180.d ./Core/Src/bmp180c/BMP180.o ./Core/Src/bmp180c/BMP180.su

.PHONY: clean-Core-2f-Src-2f-bmp180c

